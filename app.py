from flask import Flask

app = Flask(__name__)

@app.route("/")
def hello_world():
    return "<h1>Hello, World!</h1>"

@app.route("/calc")
def calc():
    a = int(request.args.get('a'))
    b = int(request.args.get('b'))
    return str(sum(a,b))

def sum(a,b):
    return a+b